\section{\texttt{save-excursion}}\etchs{3}{10}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PETITES MACROS LOCALES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\tsv}{\tm{save-excursion}\xspace}
\newcommand{\sv}{save-excursion}


La fonction \tsv est la troisième et dernière forme
spéciale dont nous allons discuter dans ce chapitre.

Dans les programmes Emacs Lisp utilisés pour l'édition, la fonction
\tsv est très commune. Elle enregistre l'emplacement du
point et la marque, exécute le corps de la fonction, puis restaure le
point et la marque sur leurs positions antérieures si leurs
emplacements ont été modifiés. Son but principal est d'empêcher
l'utilisateur d'être surpris et troublé par le mouvement inattendu du
point ou de la marque.

Avant de discuter de \tsv, cependant, il peut être
utile de revoir d'abord ce que sont le point et la marque dans GNU
Emacs. Le \textit{point} est la position actuelle du curseur. Plus
précisément, sur les terminaux où le curseur semble être sur le dessus
d'un caractère, le point est immédiatement avant le caractère. En
Emacs Lisp, le point est un entier. Le premier caractère dans un
tampon est le numéro un, le second est le numéro deux, et ainsi de
suite. La fonction \tm{point} renvoie la position actuelle du curseur
comme nombre. Chaque tampon a sa propre valeur pour le point.

La \tm{marque} est une autre position dans le tampon; sa valeur peut
être réglée avec une commande telle que \tm{\textit{C-SPC}
  (set-mark-command)}. Si une marque a été défini, vous pouvez
utiliser la commande \tm{\textit{C-x C-x} (exchange-point-and-mark)}
pour amener le curseur à sauter jusqu'à la marque et définir la marque
pour être la position antérieure du point. En outre, si vous
définissez une autre marque, la position de la marque précédente est
enregistrée dans le cycle de marquage. De nombreuses positions de
marques peuvent être sauvegardées de cette façon. Vous pouvez faire
sauter le curseur vers une marque enregistrée en tapant
\tm{\textit{C-u C-SPC}} une ou plusieurs fois.

La partie du tampon entre le point et la marque est appelée la
\textit{région}. De nombreuses commandes fonctionnent sur la région,
incluant \tm{center-region, count-lines region, kill-region} et
\tm{print-region}. 

La forme spéciale \tsv enregistre les emplacements du
point et de la marque et les restitue après le code dans le corps de
la forme spéciale évaluée par l'interprète Lisp. Ainsi, si le point
était au début d'un morceau de texte et qu'un code ait déplacé le
point à la fin du tampon, la fonction \tsv remettrait
le point où il était avant, après les expressions dans le corps de la
fonction qui était évaluée.

Dans Emacs, une fonction déplace fréquemment le point dans le cadre de
son fonctionnement interne, même si un utilisateur ne s'attendrait pas
à cela. Par exemple, \tm{count-lines-region} déplace le point. Pour
empêcher l'utilisateur d'être gêné par ces sauts qui sont à la fois
inattendus et (du point de vue de l'utilisateur) inutiles,
\tsv est souvent utilisée pour maintenir le point et la
marque à l'emplacement prévu par l'utilisateur. L'utilisation de
\tsv fait un bon entretien de la maison.

Pour vous assurer que la maison reste propre, \tm{save-excurion}
restaure les valeurs du point et de la marque, même si quelque chose
va mal dans le code à l'intérieur de celui-ci (ou, pour être précis et
utiliser le jargon approprié, << en cas de sortie anormale >> ). Cette
fonction est très utile. 

En plus d'enregistrer les valeurs du point et de la marque,
\tsv garde les traces du tampon courant, et les
restaure aussi. Cela signifie que vous pouvez écrire du code qui va
changer le tampon et avoir \tsv permute pour revenir au
tampon original. Voilà comment \tsv est utilisé dans
\tm{append-to-buffer}. (Voir Section \cfchs{4}{4} <<La définition de
\tm{append-to-buffer}>>, page \cfchsg{4}{4}.) 

\subsection{Modèle pour une expression
  \texttt{save-excursion}}\etchss{3}{10}{1} 

Le modèle pour un code utilisant \tsv est simple :

\tm{(\sv}

\tm{\textit{body}\dots}

Le corps de la fonction est une ou plusieurs expressions qui seront
évaluées en séquence par l'interprète Lisp. S'il y a plus d'une
expression dans le corps, la valeur de la dernière sera renvoyée en
tant que valeur de la fonction \tsv. Les autres expressions dans le
corps sont évaluées seulement pour leurs effets secondaires; et \tsv
elle-même est utilisée seulement pour ses effets secondaires (qui est
de restaurer les positions de point et de la marque). 

Pour être plus précis, le modèle pour une expression \tsv ressemble à
ceci :

\tm{(\sv}

\tm{\textit{first-expression-in-body}}

\tm{\textit{second-expression-in-body}}

\tm{\textit{third-expression-in-body}}

\tm{\dots}

\tm{\textit{last-expression-in-body})}

Une expression, bien sûr, peut être un symbole en soi ou une liste.

Dans le code Emacs Lisp, une expression \tsv apparaît souvent dans le
corps d'une expression \tm{let}. Cela ressemble à ceci :

\tm{(let \textit{varlist}}

\tm{(\textit{\sv}}

\tm{\textit{body}\dots}

Dans les derniers chapitres, nous avons introduit une macro et un bon
nombre de fonctions et de formes spéciales. Ici, elles sont décrites en
bref, avec quelques fonctions similaires qui n'ont pas encore été
mentionnées.

\begin{center}
  \begin{tabular}[m]{>{\ttfamily}lp{8cm}}
    eval-last-sexp & \'Evaluer la dernière expression symbolique avant
                     l'emplacement actuel du point. La valeur est
                     imprimée dans la zone écho à moins que la
                     fonction ne soit appelée avec un argument; dans
                     ce cas, la sortie est imprimée dans le tampon
                     courant. Cette commande est normalement liée à
                     \tm{\textit{C-x C-e}}. \\
    defun & Définition de la fonction. Cette macro a jusqu'à cinq
            parties : le nom, un modèle pour les arguments qui seront
            passés à la fonction, la documentation, une déclaration
            facultative interactive, et le corps de la définition. \\
          & Par exemple, dans une première version d'Emacs, la
            définition de fonction était comme suit. (C'est un peu
            plus complexe maintenant que ça cherche le premier
            caractère non-blanc plutôt que le premier caractère
            visible.) \\
          &
            \begin{minipage}[m]{1.0\linewidth}
              {\ttfamily (defun back-to-indentation ()

                 ``Move point to first visible character on line.''

                 (interactive)

                 (beginning-of-line 1)

                 (skip-chars-forward `` \textbackslash t''))}
            \end{minipage}\\
    interactive & Déclarer à l'interprète que la fonction peut être
                  utilisée de manière inteactive. Cette forme spéciale
                  peut être suivie par une chaîne avec une ou
                  plusieurs parties qui passent les informations aux
                  arguments de la fonction, en séquence. Ces pièces
                  peuvent également dire à l'interprète pour demander
                  des informations. Ces parties de la chaîne sont
                  séparées par des sauts de ligne, \tm{'\textbackslash
                  n'}.\\
                & Les caractères de code courant sont : \\
                &
                  \begin{minipage}[m]{1.0\linewidth}
                    \begin{tabular}[m]{>{\ttfamily}l}
                      b & Le nom d'un tampon existant. \\
                      f & Le nom d'un fichier existant. \\
                      p & L'argument numérique préfixé. (Notez que
                          ce \tm{'p'} est en minuscule.)\\
                      r & Le point et la marque, comme deuxarguments
                          numériques, le plus petit d'abord. 
                    \end{tabular}
                  \end{minipage}\\
                & Voir section <<Caractères de code pour
                  \tm{'interactive'}>> dans \textit{le Manuel de Référence GNU
                  Emacs Lisp}, pour une liste complète des caractères
                  de code. \\
    let & Déclare que la liste de variables est pour une utilisation
          dans le corps de \tm{let} et leur donne une valeur initiale,
          soit \tm{nil} soit une valeur spécifiée; ensuite évalue le
          reste des expressions dans le corps de \tm{let},
          l'interprète Lisp ne voit pas les valeurs des variables des
          mêmes noms qui sont liées à l'extérieur de \tm{let}. \\
        & Par exemple, 
          \begin{minipage}[m]{1.0\linewidth}
            {\ttfamily (let ((foo (buffer-name))
                             (bar (buffer-size)))
                         (message
                         ``This buffer is \%s and has \%d
                         characters.''
                         foo bar))}
          \end{minipage}\\
    \sv & Enregistre les valeurs du point et de la marque et le tampon
          courant avant d'évaluer le corps de cette forme
          spéciale. Restaure les valeurs du point et de la marque et
          le tampon ensuite. \\
        & Par exemple,
          \begin{minipage}[m]{1.0\linewidth}
            {\ttfamily 
              (message ``We are \%d characters into this buffer.''
                       (- (point)
                          (\sv
                            (goto-char (point-min)) (point))))
                          }
          \end{minipage}\\
    if & \'Evalue le premier argument de la fonction; s'il est vrai,
         évalue le second argument; sinon évalue le troisième
         argument, s'il y en a un.\\
       & La forme spéciale \tm{if} est appelée une
         \textit{conditionnelle}. Il y a d'autres conditionnelles en
         Emacs Lisp, mais \tm{if} est peut-être la plus couramment
         utilisée.\\
       & Par exemple,
         \begin{minipage}[m]{1.0\linewidth}
           {\ttfamily
             (if (= 22 emacs-major-version)
                 (message ``This is version 22 Emacs'')
               (message ``This is not version 22 Emacs''))
             }
         \end{minipage}\\
    < & \\
    > & \\
    <=& \\
    >=& La fonction \tm{<} teste si son premier argument est plus petit
        que son second. Idem pour \tm{>}. De même pour \tm{<=} et
        \tm{>=}. Dans tous les cas, les deux arguments doivent être
        des nombres ou des marqueurs (les marqueurs indiquent les
        positions dans les tampons).\\
    =& La fonction \tm{=} teste si les deux arguments, tous deux
       numériques ou marqueurs, sont égaux. \\
    equal& \\
    eq & Teste si deux objets sont les mêmes. \tm{equal} utilise un
         sens du mot <<même>> et \tm{eq} utilise un autre : \tm{equal}
         renvoie vrai si les deux objets ont une structure similaires
         et même contenus, telles deux copies du même livre. D'autre
         part, \tm{eq}, renvoie vrai si les deux arguments sont en
         fait le même objet.\\
    string<&\\
    string-lessp&\\
    string=&\\
    string-equal&La fonction \tm{string-lessp} teste si son premier
                  argument est plus petit que son second. Un diminutif
                  pour la même fonction (un \tm{defalias}) est
                  \tm{string<}.\\
                & Les arguments de \tm{string-lessp} doivent être des
                  chaînes ou des symboles; l'ordre est
                  lexicographique, donc la casse est
                  importante. Les noms des symboles sont utilisés au
                  lieu des symboles eux-mêmes. \\
                & Une chaîne vide <<"">>, une chaîne sans caractère à
                  l'intérieur, est plus petite qu'une chaîne avec des
                  caractères. \\
                & \tm{string-equal} fournit le test correspondant pour
                  l'égalité. C'est un diminutif pour \tm{string=}. Il
                  n'y a pas de fonction test correspondant à \tm{>,>=}
                  ou \tm{<=}.\\
    message     & Imprime un message dans la zone écho. Le premier
                  argument est une chaîne qui peut contenir des
                  \tm{'\%s'}, \tm{'\%d'}, ou \tm{'\%c'} pour imprimer
                  la valeur des arguments qui suivent la
                  chaîne. L'argument utilisé par \tm{'\%s'} doit être
                  un nombre. L'argument utilisé par \tm{'\%c'} doit
                  être un code ASCII; il sera imprimé comme un
                  caractère avec ce code ASCII. (Diverses autres
                  \%-séquences ne sont pas mentionnées).\\
    setq & \\
    set & 
  \end{tabular}
\end{center}


