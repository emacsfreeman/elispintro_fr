\section{\texttt{let}}\etchs{3}{6}

L'expression \tm{let} est une forme spéciale en Lisp que vous aurez
besoin d'utiliser dans la plupart des définitions de fonctions.

\tm{let} est utilisé pour attacher ou lier un symbole à une valeur
d'une telle manière que l'interprète Lisp ne confondra pas la variable
avec une variable du même nom qui ne fait pas partie de la fonction.

Pour comprendre pourquoi la forme spéciale \tm{let} est nécessaire,
examinez la situation dans laquelle seriez le propriétaire d'une
maison à laquelle vous vous référez généralement comme <<la maison>>,
comme dans la phrase, <<La maison a besoin de peinture.>> Si vous
visitez un ami et que votre hôte se réfère à <<la maison>>, il est
susceptible de se référer à \textit{sa} maison, pas la vôtre, qui est,
une maison différente.

Si votre ami se réfère à sa maison et que vous pensez qu'il fait
référence à votre maison, vous pourriez être dans une certaine
confusion. La même chose pourrait se produire dans Lisp si une
variable qui est utilisée à l'intérieur d'une fonction a le même nom
qu'une variable qui est utilisée à l'intérieur d'une autre fonction,
et les deux ne sont pas destinées à se référer à la même valeur. La
forme spéciale \tm{let} empêche ce genre de confusion.

La forme spéciale \tm{let} empêche la confusion. \tm{let} crée un nom
pour une variable \textit{locale} qui éclipse toute utilisation du même nom en
dehors de l'expression \tm{let}. Cela ressemble à la compréhension que
chaque fois que votre hôte se réfère à <<la maison>>, il signifie sa
maison, pas la vôtre. (Les symboles utilisés dans les listes
d'arguments fonctionnent de la même façon. Voir la section
\cfchs{3}{1} <<La Macro \tm{defun}>>, page \cfchsg{3}{1}.)

Les variables locales créées par une expression \tm{let} conservent
leur valeur \textit{uniquement} dans l'expression \tm{let} elle-même
(et dans des expressions appelées au sein de l'expression \tm{let});
les variables locales n'ont aucun effet en dehors de l'expression
\tm{let}.

Une autre façon de penser à propos de \tm{let} est qu'il est comme un
\tm{setq} qui est temporaire et local. Les valeurs fixées par \tm{let}
sont automatiquement annulées lorsque \tm{let} est terminé. Le
paramètre affecte uniquement les expressions qui sont à l'intérieur
des limites de l'expression \tm{let}. En jargon informatique, nous
dirions que <<la liaison d'un symbole est visible uniquement dans les
fonctions appelées dans une forme \tm{let}; en Emacs Lisp, le cadrage
est dynamique, et non lexical.>>

\tm{let} peut créer plus d'une variable à la fois. Aussi, \tm{let}
donne à chaque variable qu'il crée une valeur initiale, soit une
valeur spécifiée par vous, soit \tm{nil}. (Dans le jardon, ça
s'appelle <<lier la variable à la valeur>>.) Après que \tm{let} ait créé et
lié les variables, ça exécute le code dans le corps de \tm{let}, et
renvoie la valeur de l'expression \tm{let}. (<<Exécute>> est un terme
de jargon qui signifie évaluer une liste, il vient de l'utilisation du
sens du mot <<pour donner un effet pratique>> (\textit{Oxford English
  Dictionary}). Puisque vous évaluez une expression pour effectuer une
action, <<exécuter>> a évolué comme un synonyme de <<évaluer>>.)

\subsection{Les parties d'une expression \texttt{let}}\etchss{3}{6}{1}

Une expression \tm{let} est une liste de trois parties. La première
partie est le symbole \tm{let}. La deuxième partie est une liste, dite
\textit{varlist}, dont chaque élément élément est soit un symbole
lui-même ou par une liste de deux éléments, le premier élément est un
symbole. La troisième partie de l'expression \tm{let} est le corps du
\tm{let}. Le corps se décompose généralement d'une ou de plusieurs
listes. 

\begin{center}
  Un modèle pour une expression \tm{let} ressemble à ça :

  \tm{(let \textit{varlist body}\dots)}
\end{center}
Les symboles de la varlist sont les variables qui ont reçu leurs
valeurs initiales par la forme spéciale \tm{let}. Les symboles par
eux-mêmes ont pour valeurs initiales \tm{nil}; et chaque symbole qui
est le premier élément d'une liste à deux éléments est lié à la valeur
qui est renvoyée quand l'interprète Lisp évalue le second élément.

Ainsi, une varlist pourrait ressembler à ceci : \tm{(thread (needles
  3))}. Dans ce cas, dans une expression \tm{let}, Emacs lie le
symbole \tm{thread} à une valeur initiale de \tm{nil}, et lie le
symbole \tm{needles} à une valeur initiale de 3.

Lorsque vous écrivez une expression \tm{let}, ce que vous faites c'est
de mettre les expressions appropriées dans les fentes du modèle
d'expression \tm{let}.

Si la varlist est composée de listes à deux éléments, comme cela est
souvent le cas, le modèle de l'expression \tm{let} ressemble à ceci :
\begin{center}
  \tm{(let ((\textit{variable value)}}

    \tm{(\textit{variable value})}

    \tm{\dots)}

    \tm{\textit{body}\dots)}
\end{center}

\subsection{\'Echantillon d'expression \texttt{let}}\etchss{3}{6}{2}

L'expression suivante crée et donne des valeurs initiales aux deux
variables zèbres et tigres. Le corps de l'expression \tm{let} est une
liste qui appelle la fonction \tm{message}.

\begin{center}
  \tm{(let ((zebra 'stripes)}

  \tm{(tiger 'fierce))}

  \tm{(message ``One kind of animal has \%s and another is \%s.'')}

  \tm{zebra tiger))}
\end{center}

Ici, la varlist est \tm{((zebra 'stripes) (tiger 'fierce))}.

Les deux variables sont \tm{zebra} et \tm{tiger}. Chaque variable est
le premier élément d'une liste à deux éléments et chaque valeur est le
deuxième élément de sa liste à deux éléments. Dans la varlist, Emacs
lie la variable \tm{zebra} à la valeur \tm{stripes}\footnote{Selon
  Jared Diamond dans \textit{Guns, Germs, and Steel}, <<\dots les
  zèbres deviennent incroyablement dangereux à mesure qu'ils
  vieillissent>>, mais la demande ici est qu'ils ne deviennent pas
  féroce comme un tigre. (1997, W. W. Norton and Co., ISBN
  0-393-03894-2, page 171)}, et lie
la variable \tm{tiger} à la valeur \tm{fierce}/ Dans cet exemple, les
deux valeurs sont des symboles précédés d'une apostrophe. Les valeurs
pourraient tout aussi bien avoir été une autre liste ou une chaîne. Le
corps du \tm{let} suit après la liste contenant les variables. Dans
cet exemple, le corps est une liste qui utilise la fonction
\tm{message} pour afficher une chaîne dans la zone d'écho.

Vous pouvez évaluer l'exemple de la manière habituelle, en plaçant le
curseur après la dernière parenthèse et en tapant \tm{C-x
  C-e}. Lorsque vous faites cela, le message suivant apparaît dans la
zone d'écho:
\begin{center}
  \tm{``One kind of animal has stripes and another is fierce.''}
\end{center}

Comme nous l'avons vu auparavant, la fonction \tm{message} imprime son
premier argument, sauf pour \tm{'\%s'}. Dans cet exemple, la valeur de
la variable \tm{zebra} est imprimée à l'emplacement du premier
\tm{'\%s'} et la valeur de la variable \tm{tiger} est imprimée à
l'emplacement du second \tm{'\%s'}.

\subsection{Variables non initialisées dans une instruction
  \texttt{let}}\etchss{3}{6}{3} 

Si vous ne liez pas les variables dans une instruction \tm{let} aux
valeurs initiales spécifiques, elles seront automatiquement liées à
une valeur initiale de \tm{nil}, comme dans l'expression suivante :
\begin{center}
  \tm{(let ((birch 3)}

  \tm{pine}

  \tm{fir}

  \tm{(oak 'some))}

  \tm{(message}

  \tm{``Here are \%d variables with \%s, \%s, and \%s value.''}

  \tm{birch pine fir oak))}
\end{center}

Ici, la varlist est \tm{((birch 3) pine fir (oak 'some))}.

Si vous évaluez cette expression de la manière habituelle, le message
suivant apparaîtra dans votre zone d'écho :
\begin{center}
  \tm{``Here are \%d variables with nil, nil, and some value.''}
\end{center}

Dans cet exemple, Emacs lie le symbole \tm{birch} au nombre \tm{3},
lie les symboles \tm{pine} et \tm{fir} à \tm{nil}, et lie le symbole
\tm{oak} à la valeur \tm{some}.

Notez que dans la première partie du \tm{let}, les variables \tm{pine}
et \tm{fir} sont les seules comme atomes qui ne sont pas entourées de
parenthèses; c'est parce qu'elles sont liées à \tm{nil}, la liste
vide. Mais \tm{oak} est lié à \tm{some} et donc est une partie de la
liste \tm{oak 'some)}. De même, \tm{birch} est lié au nombre \tm{3} et
donc est dans une liste avec un nombre. (Puisqu'un nombre
s'auto-évalue, le nombre n'a pas besoin d'apostrophe. Aussi, le nombre
est imprimé dans le message en utilisant \tm{'\%d'} au lieu de
\tm{'\%s'}.) Les quatre variables en tant que groupe sont mises en une
liste pour les délimiter du corps du \tm{let}.