\section{La forme spéciale \tm{if}}\etchs{3}{7}

Une troisième forme spéciale, en plus de \tm{defun} et \tm{let}, est
la conditionnelle \tm{if}. Cette forme est utilisée pour demander à
l'ordinateur de prendre des décisions. Vous pouvez écrire les
définitions de fonctions sans utiliser \tm{if}, mais elle est utilisée
assez souvent, et est suffisamment importante pour être incluse
ici. Elle est utilisée, par exemple, dans le code de la fonction
\tm{beginning-of-buffer}.

L'idée de base derrière un \tm{if}, est que <<\textit{si} un test est
vraie, \textit{alors} l'expression est évaluée.>>  Si le test n'est
pas vrai, l'expression n'est pas évaluée. Par exemple, vous pourriez
prendre une décision telle que, <<si il fait beau et chaux, alors
j'irai à la plage !>> 

Une expression \tm{if} écrite en Lisp n'utilise pas le mot <<alors>>;
le test et l'action sont les second et troisième éléments de la liste
dont le premier élément est \tm{if}. 

Néanmoins, la partie de test d'une expression \tm{if} est souvent
appelée la \textit{partie si} et le second argument est souvent appelé
la \textit{partie alors}.

Aussi, quand une expression \tm{if} est écrite, le test vrai ou faux
est généralement écrit sur la même ligne que le symbole \tm{if}, mais
l'action à effectuer si le test est vrai, la <<partie alors>>, est
écrite sur la deuxième ligne et les suivantes. Cela rend l'expression
\tm{if} plus facile à lire.

\begin{center}
  \tm{(if \textit{true-or-false-test}}

  \tm{\textit{action-to-carry-out-if-test-is-true})}
\end{center}

Le test vrai ou faux sera une expression qui est évaluée par
l'interprète Lisp. Voici un exemple que vous pouvez évaluer de la
manière habituelle. Le test est de savoir si le nombre 5 est supérieur
au nombre 4. Comme il l'est, le message <<\tm{5 is greater than 4!}>>
sera affiché.

\begin{center}
  \tm{(if (> 5 4)                  ; if-part}

  \tm{(message ``5 is greater than 4!'')) ; then-part}
\end{center}

(La fonction \tm{>} teste si son premier argument est supérieur à son
second et renvoie vrai si c'est le cas.)

Bien sûr, en utilisation réelle, le test dans une expression \tm{if}
ne sera pas fixé une fois pour toutes comme c'est le cas dans
l'expression \tm{(> 5 4)}. Au lieu de cela, au moins l'une des
variables utilisées dans le test sera liée à une valeur qui n'est pas
connue à l'avance. (Si la valeur était connue à l'avance, nous
n'aurions pas besoin de faire un test!)

Par exemple, la valeur peut être liée à un argument d'une définition
de fonction. Dans la définition de la fonction suivante, le caractère
de l'animal est une valeur qui est transmise à la fonction. Si la
valeur liée \tm{characteristic} est \tm{fierce}, alors le message,
<<\tm{It's a tiger!}>> sera affiché; sinon, \tm{nil} sera renvoyé.

\tm{(defun type-of-animal (characteristic)}

 \tm{ "Print message in echo area depending on CHARACTERISTIC.}

\tm{If the CHARACTERISTIC is the symbol 'fierce', }

\tm{then warn of a tiger."}

\tm{  (if (equal characteristic 'fierce)}

\tm{      (message "It's a tiger!")))}

Si vous lisez ceci à l'intérieur de GNU Emacs, vous pouvez évaluer la
définition de la fonction de la manière habituelle et l'installer dans
Emacs, puis vous pouvez évaluer les deux expressions suivantes pour
voir les résultats :

\tm{(type-of-animal 'fierce)}

\tm{(type-of-animal 'zebra)}

Lorsque vous évaluez \tm{(type-of-animal 'fierce)}, vous verrez le
message suivant imprimé dans la zone écho : <<\tm{It's a tiger!}>>; et
lorsque vous évaluez \tm{(type-of-animal 'zebra)}, vous verrez
\tm{nil} qui s'imprimera dans la zone écho.

\subsection{La fonction \texttt{type-of-animal} en
  détail}\etchss{3}{7}{1}

Regardons la fonction \tm{type-of-animal} en détail.

La définition de fonction pour \tm{type-of-animal} a été écrite en
remplissant les trous de deux modèles, l'un pour la définition de
fonction dans son ensemble, et le second pour l'expression \tm{if}.

Le modèle pour chaque fonction qui n'est pas interactive est : 

\tm{(defun \textit{name-of-function (argument-list)}}

\tm{\textit{``documentation\dots''}}

\tm{\textit{body\dots)}}

Les parties de la fonction qui correspondent au modèle ressemble à
ceci :

\tm{(defun type-of-animal (characteristic)}

\tm{``Print message in echo area depending on CHARACTERISTIC.}

\tm{If the CHARACTERISTIC is the symbol 'fierce',}

\tm{then warn of a tiger.''}

\tm{\textit{body: the if expression)}}

Le nom de la fonction est \tm{type-of-animal}; il reçoit la valeur
d'un argument. La liste d'argument est suivie par une
documentation. La documentation est incluse dans l'exemple parce que
c'est une bonne habitude d'écrire une documentation pour chaque
fonction. Le corps de la définition de fonction est constitué de
l'expression \tm{if}.

Le modèle d'une expression \tm{if} ressemble à ceci :

\tm{(if \textit{true-or-false-test}}

\tm{\textit{action-to-carry-out-if-the-test-returns-true)}}

Dans la fonction \tm{type-of-animal}, le code pour le \tm{if}
ressemble à ceci :

\tm{(if (equal characteristic 'fierce)}

\tm{(message ``It's a tiger!'')))}

Ici le test vrai ou faux est l'expression :

\tm{(equal characteristic 'fierce)}

En Lisp, \tm{equal} est une fonction qui détermine si le premier
argument est égal au second. Le second arguemnt est le symbole quoté
\tm{'fierce} et le premier est la valeur du symbole
\tm{characteristic}---en d'autres mots, l'argument passé à la
fonction.

Dans le premier exercice \tm{type-of-animal}, l'argument \tm{fierce}
est passé à \tm{type-of-animal}. Puisque \tm{fierce} est égal à
\tm{fierce}, l'expression, \tm{(equal characteristic 'fierce)},
renvoie une valeur vraie. Lorsque cela arrive, le \tm{if} évalue le
second argument ou la partie alors du \tm{if} : \tm{(message ``It's
  tiger!'')}.

D'autre part, dans le second exercice de \tm{type-of-animal},
l'argument \tm{zebra} est passé à \tm{type-of-animal}. \tm{zebra}
n'est pas égal à \tm{fierce}, donc la partie alors n'est pas évaluée
et \tm{nil} est renvoyé par l'expression \tm{if}.


